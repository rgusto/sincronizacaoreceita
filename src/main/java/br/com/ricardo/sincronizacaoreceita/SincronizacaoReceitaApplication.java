package br.com.ricardo.sincronizacaoreceita;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.opencsv.CSVWriter;

import br.com.ricardo.sincronizacaoreceita.service.ReceitaService;

@SpringBootApplication
public class SincronizacaoReceitaApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(SincronizacaoReceitaApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		// lendo o arquivo e armazenando numa lista
		System.out.println("Lendo o arquivo...");
		List<List<String>> records = new ArrayList<>();
		try (BufferedReader br = new BufferedReader(new FileReader("envio.csv"))) {
			String line;
			while ((line = br.readLine()) != null) {
				String[] values = line.split(";");
				records.add(Arrays.asList(values));
			}
		}

		// criando o arquivo para gravar o resultado do envio da atualização da receita
		FileWriter outputfile = new FileWriter(new File("recebimento.csv"));
		CSVWriter writer = new CSVWriter(outputfile, ';', CSVWriter.NO_QUOTE_CHARACTER,
				CSVWriter.DEFAULT_ESCAPE_CHARACTER, CSVWriter.DEFAULT_LINE_END);

		List<String[]> data = new ArrayList<String[]>();

		data.add(new String[] { "agencia", "conta", "saldo", "status", "resultado" });

		// percorrendo o arquivo de envio e chamando o "Serviço da Receita" para gravar
		// o resultado no arquivo de recebimento
		System.out.println("Enviando as informações...");
		for (int i = 1; i < records.size(); i++) {

			ReceitaService receitaService = new ReceitaService();
			boolean retorno = receitaService.atualizarConta(records.get(i).get(0), records.get(i).get(1),
					Double.parseDouble(records.get(i).get(2)), records.get(i).get(3));

			data.add(new String[] { records.get(i).get(0), records.get(i).get(1), records.get(i).get(2),
					records.get(i).get(3), Boolean.toString(retorno) });

		}

		writer.writeAll(data);
		writer.close();
		System.out.println("Informações enviadas com sucesso.");
		
	}

}
